package mainvoker.makech.io.test.logic;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import mainvoker.makech.io.test.models.UsersList;
import mainvoker.makech.io.test.db.SQLiteHelper;
import mainvoker.makech.io.test.db.UserTable;

/**
 * Created by MAInvoker on 07/05/2019.
 */

public class UsersLogic {
    Context App_Context;
    public UsersLogic(Context context){
        App_Context = context;
    }

    public ArrayList<UsersList> GetActiveUsers(){
        SQLiteHelper helper = new SQLiteHelper(App_Context);
        SQLiteDatabase db = helper.getReadableDatabase();

        ArrayList<UsersList> active_users = new ArrayList<>();

        String WHERE_CONDITION = UserTable.UserTableEntry.COLUMN_NAME_STATUS + " = ? ";
        String[] WHERE_ARGS = { "1" };
        String sortOrder =
                UserTable.UserTableEntry._ID + " DESC";

        Cursor cursor = db.query(
                UserTable.UserTableEntry.TABLE_NAME,
                null,
                WHERE_CONDITION,
                WHERE_ARGS,
                null,
                null,
                sortOrder
        );

        if(cursor.moveToFirst()){
            do {
                active_users.add(new UsersList(cursor.getInt(cursor.getColumnIndex(UserTable.UserTableEntry._ID)),
                        cursor.getString(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_FIRST_NAME)),
                        cursor.getString(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_LAST_NAME)),
                        cursor.getString(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_CREATED_ON)),
                        cursor.getString(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_DEACTIVATED_ON)),
                        cursor.getInt(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_STATUS))));
            }
            while (cursor.moveToNext());
        }
        db.close();
        return active_users;
    }

    public UsersList GetUsersByID(int id){
        SQLiteHelper helper = new SQLiteHelper(App_Context);
        SQLiteDatabase db = helper.getReadableDatabase();

        UsersList user = null;

        String WHERE_CONDITION = UserTable.UserTableEntry._ID + " = ? ";
        String[] WHERE_ARGS = { id+"" };
        Cursor cursor = db.query(
                UserTable.UserTableEntry.TABLE_NAME,
                null,
                WHERE_CONDITION,
                WHERE_ARGS,
                null,
                null,
                null
        );

        if(cursor.moveToFirst()){
            do {
                user = new UsersList(cursor.getInt(cursor.getColumnIndex(UserTable.UserTableEntry._ID)),
                        cursor.getString(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_FIRST_NAME)),
                        cursor.getString(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_LAST_NAME)),
                        cursor.getString(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_CREATED_ON)),
                        cursor.getString(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_DEACTIVATED_ON)),
                        cursor.getInt(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_STATUS)));
            }
            while (cursor.moveToNext());
        }
        db.close();
        return user;
    }

    public ArrayList<UsersList> GetDeactivatedUsers(){

        SQLiteHelper helper = new SQLiteHelper(App_Context);
        SQLiteDatabase db = helper.getReadableDatabase();

        ArrayList<UsersList> active_users = new ArrayList<>();

        String WHERE_CONDITION = UserTable.UserTableEntry.COLUMN_NAME_STATUS + " = ? ";
        String[] WHERE_ARGS = { "0" };
        String sortOrder =
                UserTable.UserTableEntry._ID + " DESC";

        Cursor cursor = db.query(
                UserTable.UserTableEntry.TABLE_NAME,
                null,
                WHERE_CONDITION,
                WHERE_ARGS,
                null,
                null,
                sortOrder
        );

        if(cursor.moveToFirst()){
            do {
                active_users.add(new UsersList(cursor.getInt(cursor.getColumnIndex(UserTable.UserTableEntry._ID)),
                        cursor.getString(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_FIRST_NAME)),
                        cursor.getString(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_LAST_NAME)),
                        cursor.getString(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_CREATED_ON)),
                        cursor.getString(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_DEACTIVATED_ON)),
                        cursor.getInt(cursor.getColumnIndex(UserTable.UserTableEntry.COLUMN_NAME_STATUS))));
            }
            while (cursor.moveToNext());
        }
        db.close();
        return active_users;
    }

    public void SaveUser(String first_name, String last_name, String created_on, String deactivated_on,boolean status){
        SQLiteHelper helper = new SQLiteHelper(App_Context);
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserTable.UserTableEntry.COLUMN_NAME_FIRST_NAME, first_name);
        values.put(UserTable.UserTableEntry.COLUMN_NAME_LAST_NAME, last_name);
        values.put(UserTable.UserTableEntry.COLUMN_NAME_CREATED_ON, created_on);
        values.put(UserTable.UserTableEntry.COLUMN_NAME_DEACTIVATED_ON, deactivated_on);
        values.put(UserTable.UserTableEntry.COLUMN_NAME_STATUS, status);
        db.insert(UserTable.UserTableEntry.TABLE_NAME, null, values);
        db.close();
    }

    public void EditUser(String id, String first_name, String last_name,String created_on, String deactivated_on, boolean status){
        SQLiteHelper helper = new SQLiteHelper(App_Context);
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserTable.UserTableEntry.COLUMN_NAME_FIRST_NAME, first_name);
        values.put(UserTable.UserTableEntry.COLUMN_NAME_LAST_NAME, last_name);
        values.put(UserTable.UserTableEntry.COLUMN_NAME_CREATED_ON, created_on);
        values.put(UserTable.UserTableEntry.COLUMN_NAME_DEACTIVATED_ON, deactivated_on);
        values.put(UserTable.UserTableEntry.COLUMN_NAME_STATUS, status);

        String where = UserTable.UserTableEntry._ID + " = ?";
        String[] whereArgs = { id };

        db.update(
                UserTable.UserTableEntry.TABLE_NAME,
                values,
                where,
                whereArgs);
        db.close();
    }
}
