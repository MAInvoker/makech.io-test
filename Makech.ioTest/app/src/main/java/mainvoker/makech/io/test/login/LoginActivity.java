package mainvoker.makech.io.test.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import mainvoker.makech.io.test.activities.HomeActivity;
import mainvoker.makech.io.test.logic.LoginLogic;
import mainvoker.makech.io.test.R;

public class LoginActivity extends AppCompatActivity {

    EditText Edt_User, Edt_Pass;
    Button Btn_Login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Edt_User = findViewById(R.id.edt_user_name);
        Edt_Pass = findViewById(R.id.edt_password);
        Btn_Login = findViewById(R.id.buttonLogin);

        Btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginLogic logic = new LoginLogic(getApplicationContext());
                if(logic.isValidUser(Edt_User.getText().toString(), Edt_Pass.getText().toString())){
                    Toast.makeText(getApplicationContext(),getString(R.string.login_success_message) + " " +Edt_User.getText().toString()+ "!", Toast.LENGTH_LONG).show();
                    Intent home = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(home);
                }else{
                    Toast.makeText(getApplicationContext(),getString(R.string.login_not_success_message), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Edt_Pass.setText("");
        Edt_User.setText("");
    }
}
