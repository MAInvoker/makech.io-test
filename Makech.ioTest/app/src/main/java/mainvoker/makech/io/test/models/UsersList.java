package mainvoker.makech.io.test.models;

import java.util.Date;

/**
 * Created by MAInvoker on 07/05/2019.
 */

public class UsersList {
    int Id;
    String FirstName;
    String LastName;
    String CreatedOn;
    String DeactivatedOn;
    int Status;

    public UsersList(int id, String firstName,String lastName,String createdOn,String deactivatedOn,int status ){
        Id = id;
        FirstName = firstName;
        LastName = lastName;
        CreatedOn = createdOn;
        DeactivatedOn = deactivatedOn;
        Status = status;
    }

    public int getUserId(){
        return Id;
    }

    public String getFirstName(){
        return FirstName;
    }

    public String getLastName(){
        return LastName;
    }

    public String createdOn(){
        return CreatedOn;
    }

    public String deactivatedOn(){
        return DeactivatedOn;
    }

    public int isActivated(){
        return Status;
    }
}
