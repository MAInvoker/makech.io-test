package mainvoker.makech.io.test.db;

import android.provider.BaseColumns;

/**
 * Created by MAInvoker on 07/05/2019.
 */

public final class UserTable {

    private UserTable(){

    }

    public static class UserTableEntry implements BaseColumns{
        public static final String TABLE_NAME = "users";
        public static final String COLUMN_NAME_FIRST_NAME = "first_name";
        public static final String COLUMN_NAME_LAST_NAME = "last_name";
        public static final String COLUMN_NAME_CREATED_ON = "created_on";
        public static final String COLUMN_NAME_DEACTIVATED_ON = "deactivated_on";
        public static final String COLUMN_NAME_STATUS = "status";

    }
}
