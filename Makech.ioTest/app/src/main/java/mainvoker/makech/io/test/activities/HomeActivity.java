package mainvoker.makech.io.test.activities;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import mainvoker.makech.io.test.BuildConfig;
import mainvoker.makech.io.test.R;
import mainvoker.makech.io.test.fragments.FragmentCreateForm;
import mainvoker.makech.io.test.fragments.FragmentList;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static FragmentList list_fragment;
    public static FragmentCreateForm list_create_form;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView btnSalir = findViewById(R.id.footer_item_exit);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageView backNavigationImage = navigationView.getHeaderView(0).findViewById(R.id.backNavigationImg);
        backNavigationImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        list_fragment = null;
        list_create_form = null;
        LoadUserListFragment();
        TextView versionText = findViewById(R.id.footer_item_version);
        versionText.setText(versionText.getText().toString()+" "+ BuildConfig.VERSION_NAME);
        final TextView btn_footer_tab_list = findViewById(R.id.btn_footer_tab_list);
        final TextView btn_footer_tab_form = findViewById(R.id.btn_footer_tab_form);

        btn_footer_tab_form.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTitle(getString(R.string.fragmentCreateFormTitle));
                btn_footer_tab_form.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorNavigationTabFooterSelected));
                btn_footer_tab_list.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorNavigationTabFooterNotSelected));
                LoadCreateFormFragment();
            }
        });

        btn_footer_tab_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTitle(getString(R.string.fragmentListTitle));
                btn_footer_tab_form.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorNavigationTabFooterNotSelected));
                btn_footer_tab_list.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorNavigationTabFooterSelected));
                getFragmentManager().popBackStack();
                LoadUserListFragment();
            }
        });
    }

    public void LoadUserListFragment(){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if(list_fragment==null){
            list_fragment = FragmentList.newInstance(getString(R.string.fragmentListTitle), "lst");
            ft.add(R.id.fragment_frame,list_fragment,getString(R.string.fragmentListTitle));
            ft.show(list_fragment);
            ft.commit();
        }else{
            list_fragment = FragmentList.newInstance(getString(R.string.fragmentListTitle), "lst");
            ft.replace(R.id.fragment_frame,list_fragment);
            ft.show(list_fragment);
            ft.commit();
        }

    }

    public void LoadCreateFormFragment(){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        list_create_form = FragmentCreateForm.newInstance(getString(R.string.fragmentCreateFormTitle), "crt");
        ft.replace(R.id.fragment_frame,list_create_form);
        ft.addToBackStack(getString(R.string.fragmentCreateFormTitle));
        ft.show(list_create_form);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id){
            case R.id.nav_list:
                setTitle(getString(R.string.fragmentListTitle));
                getFragmentManager().popBackStack();
                LoadUserListFragment();
                break;
            case R.id.nav_create:
                setTitle(getString(R.string.fragmentCreateFormTitle));
                LoadCreateFormFragment();
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
