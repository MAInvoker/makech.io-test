package mainvoker.makech.io.test.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import mainvoker.makech.io.test.R;
import mainvoker.makech.io.test.models.UsersList;
import mainvoker.makech.io.test.fragments.FragmentCreateForm;

/**
 * Created by MAInvoker on 07/05/2019.
 */

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<UsersList> usersLists;
    private boolean isActivatedUsersList;

    public UsersAdapter(Context context, ArrayList<UsersList> usersLists, boolean typeList){
        this.context = context;
        this.usersLists = usersLists;
        this.isActivatedUsersList = typeList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_user,txt_date;
        private ImageView thumbnail;
        private LinearLayout lv_cell;
        public ViewHolder(View v) {
            super(v);
            txt_user = v.findViewById(R.id.txt_first_name);
            txt_date = v.findViewById(R.id.txt_date);
            thumbnail = v.findViewById(R.id.img_thumbnail);
            lv_cell = v.findViewById(R.id.lv_cell_parent_user);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.active_users_cell, parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final UsersList User = usersLists.get(position);
        holder.txt_user.setText(User.getFirstName()+ " "+ User.getLastName());
        holder.thumbnail.setImageDrawable(null);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String createdOn = User.createdOn();
        String deactivatedOn = User.deactivatedOn();
        try {
            Date date = dateFormat.parse(createdOn);
            dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");
            createdOn = dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(deactivatedOn != null){
            dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date date = dateFormat.parse(deactivatedOn);
                dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");
                deactivatedOn = dateFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(isActivatedUsersList){
            holder.thumbnail.setImageResource(R.drawable.ic_action_name);
            holder.txt_date.setText(context.getString(R.string.created_text)+" "+createdOn);
        }else{
            holder.thumbnail.setImageResource(R.drawable.ic_disable);
            holder.txt_date.setText(context.getString(R.string.deactivated_text)+" "+deactivatedOn);
        }

        holder.lv_cell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                FragmentCreateForm list_create_form = FragmentCreateForm.newInstance(User.getUserId());
                activity.getFragmentManager().
                        beginTransaction().
                        replace(R.id.fragment_frame,list_create_form).
                        addToBackStack(context.getString(R.string.fragmentCreateFormTitle)).show(list_create_form).
                        commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersLists.size();
    }

    @Override
    public Filter getFilter() {
        return null;
    }
}
