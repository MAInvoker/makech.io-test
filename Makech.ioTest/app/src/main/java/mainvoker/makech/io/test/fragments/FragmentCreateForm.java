package mainvoker.makech.io.test.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import mainvoker.makech.io.test.R;
import mainvoker.makech.io.test.models.UsersList;
import mainvoker.makech.io.test.logic.UsersLogic;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentCreateForm.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentCreateForm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCreateForm extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private static int User_id = 0;
    private OnFragmentInteractionListener mListener;

    Switch sw_active;
    EditText edt_first_name;
    EditText edt_last_name;
    Button btn_save;
    Button btn_cancel;
    UsersList user;
    public FragmentCreateForm() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentCreateForm.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentCreateForm newInstance(String param1, String param2) {
        User_id = 0;
        FragmentCreateForm fragment = new FragmentCreateForm();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentCreateForm newInstance(int user_id) {
        FragmentCreateForm fragment = new FragmentCreateForm();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        User_id = user_id;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_create_form, container, false);
        sw_active = view.findViewById(R.id.switch_active);
        edt_first_name = view.findViewById(R.id.edt_first_name);
        edt_first_name.setText("");
        edt_last_name = view.findViewById(R.id.edt_last_name);
        edt_last_name.setText("");
        btn_save = view.findViewById(R.id.btn_save);
        btn_cancel = view.findViewById(R.id.btn_cancel);

        final UsersLogic logic = new UsersLogic(getActivity());
        if(User_id > 0){
            user = logic.GetUsersByID(User_id);
            if(user != null){
                getActivity().setTitle(getString(R.string.fragmentEditFormTitle));
                edt_first_name.setText(user.getFirstName());
                edt_last_name.setText(user.getLastName());
                sw_active.setChecked( user.isActivated() > 0 ? true : false );
            }
        }

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(User_id > 0 && user!=null){
                    if(!edt_first_name.getText().toString().equals("")){
                        logic.EditUser(User_id+"",
                                edt_first_name.getText().toString(),edt_last_name.getText().toString(),user.createdOn(),
                                !sw_active.isChecked() ? GetDate() : user.deactivatedOn(),sw_active.isChecked());
                        Pop();
                    }else{
                        Toast.makeText(getActivity(),getString(R.string.do_not_empty_message), Toast.LENGTH_LONG).show();
                    }
                }else{
                    if(!edt_first_name.getText().toString().equals("")){
                        logic.SaveUser(edt_first_name.getText().toString(),edt_last_name.getText().toString(),GetDate(),GetDate(),sw_active.isChecked());
                        Pop();
                    }else{
                        Toast.makeText(getActivity(),getString(R.string.do_not_empty_message), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Pop();
            }
        });

        return view;
    }

    public void Pop(){
        getActivity().getFragmentManager().popBackStack();
    }

    public String GetDate(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c);
        return formattedDate;
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
