package mainvoker.makech.io.test.db;

/**
 * Created by MAInvoker on 07/05/2019.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper{
    private static final String DB_NAME = "makechTest";
    private static final int DB_VERSION = 1;

    public SQLiteHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ROOT_LOGIN);
        sqLiteDatabase.execSQL(SQL_SET_ROOT_LOGIN_USER);
        sqLiteDatabase.execSQL(SQL_CREATE_USERS);
        sqLiteDatabase.execSQL(SQL_SET_ACTIVE_USER);
        sqLiteDatabase.execSQL(SQL_SET_ACTIVE_USER2);
        sqLiteDatabase.execSQL(SQL_SET_ACTIVE_USER3);
        sqLiteDatabase.execSQL(SQL_SET_ACTIVE_USER4);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    private static final String SQL_CREATE_USERS =
            "CREATE TABLE " + UserTable.UserTableEntry.TABLE_NAME + " (" +
                    UserTable.UserTableEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    UserTable.UserTableEntry.COLUMN_NAME_FIRST_NAME + " VARCHAR," +
                    UserTable.UserTableEntry.COLUMN_NAME_LAST_NAME + " VARCHAR, " +
                    UserTable.UserTableEntry.COLUMN_NAME_CREATED_ON + " DATETIME, " +
                    UserTable.UserTableEntry.COLUMN_NAME_DEACTIVATED_ON + " DATETIME, " +
                    UserTable.UserTableEntry.COLUMN_NAME_STATUS + " INTEGER)";

    private static final String SQL_CREATE_ROOT_LOGIN =
            "CREATE TABLE " + RootLoginTable.RootLoginEntry.TABLE_NAME + " (" +
                    RootLoginTable.RootLoginEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    RootLoginTable.RootLoginEntry.COLUMN_NAME_USER + " VARCHAR," +
                    RootLoginTable.RootLoginEntry.COLUMN_NAME_PASSWORD + " VARCHAR)";

    private static final String SQL_SET_ROOT_LOGIN_USER =
            "INSERT INTO "+ RootLoginTable.RootLoginEntry.TABLE_NAME +"("+ RootLoginTable.RootLoginEntry.COLUMN_NAME_USER +","+ RootLoginTable.RootLoginEntry.COLUMN_NAME_PASSWORD +")" +
                    " VALUES ('Admin','Makech.io')";

    private static final String SQL_SET_ACTIVE_USER =
            "INSERT INTO "+ UserTable.UserTableEntry.TABLE_NAME +"("+ UserTable.UserTableEntry.COLUMN_NAME_FIRST_NAME +","+ UserTable.UserTableEntry.COLUMN_NAME_LAST_NAME +"," +
                            UserTable.UserTableEntry.COLUMN_NAME_CREATED_ON + ","+ UserTable.UserTableEntry.COLUMN_NAME_DEACTIVATED_ON+ ","+ UserTable.UserTableEntry.COLUMN_NAME_STATUS +")" +
                    " VALUES ('Manuel','Acosta', '2019-02-02 12:04:34','2020-01-28 12:04:34','1')";

    private static final String SQL_SET_ACTIVE_USER2 =
            "INSERT INTO "+ UserTable.UserTableEntry.TABLE_NAME +"("+ UserTable.UserTableEntry.COLUMN_NAME_FIRST_NAME +","+ UserTable.UserTableEntry.COLUMN_NAME_LAST_NAME +"," +
                    UserTable.UserTableEntry.COLUMN_NAME_CREATED_ON + ","+ UserTable.UserTableEntry.COLUMN_NAME_DEACTIVATED_ON+ ","+ UserTable.UserTableEntry.COLUMN_NAME_STATUS +")" +
                    " VALUES ('Ana','Toledo', '2019-02-02 12:04:34','2020-01-28 12:04:34','1')";

    private static final String SQL_SET_ACTIVE_USER3 =
            "INSERT INTO "+ UserTable.UserTableEntry.TABLE_NAME +"("+ UserTable.UserTableEntry.COLUMN_NAME_FIRST_NAME +","+ UserTable.UserTableEntry.COLUMN_NAME_LAST_NAME +"," +
                    UserTable.UserTableEntry.COLUMN_NAME_CREATED_ON + ","+ UserTable.UserTableEntry.COLUMN_NAME_DEACTIVATED_ON+ ","+ UserTable.UserTableEntry.COLUMN_NAME_STATUS +")" +
                    " VALUES ('Fernando','Samaniego', '2019-02-02 12:04:34','2020-01-28 12:04:34','0')";

    private static final String SQL_SET_ACTIVE_USER4 =
            "INSERT INTO "+ UserTable.UserTableEntry.TABLE_NAME +"("+ UserTable.UserTableEntry.COLUMN_NAME_FIRST_NAME +","+ UserTable.UserTableEntry.COLUMN_NAME_LAST_NAME +"," +
                    UserTable.UserTableEntry.COLUMN_NAME_CREATED_ON + ","+ UserTable.UserTableEntry.COLUMN_NAME_DEACTIVATED_ON+ ","+ UserTable.UserTableEntry.COLUMN_NAME_STATUS +")" +
                    " VALUES ('Miguel','Tec', '2019-02-02 12:04:34','2020-01-28 12:04:34','0')";

}
