package mainvoker.makech.io.test.db;

import android.provider.BaseColumns;

/**
 * Created by MAInvoker on 07/05/2019.
 */

public final class RootLoginTable {

    public static class RootLoginEntry implements BaseColumns {
        public static final String TABLE_NAME = "root_login";
        public static final String COLUMN_NAME_USER = "root_user";
        public static final String COLUMN_NAME_PASSWORD = "root_password";

    }
}
