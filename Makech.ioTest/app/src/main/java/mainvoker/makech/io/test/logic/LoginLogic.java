package mainvoker.makech.io.test.logic;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import mainvoker.makech.io.test.db.RootLoginTable;
import mainvoker.makech.io.test.db.SQLiteHelper;

/**
 * Created by MAInvoker on 07/05/2019.
 */

public class LoginLogic {

    Context App_Context;
    public LoginLogic(Context context){
        App_Context = context;
    }

    public Boolean isValidUser(String user, String pass){
        SQLiteHelper helper = new SQLiteHelper(App_Context);
        SQLiteDatabase db = helper.getReadableDatabase();
        String WHERE_CONDITION = RootLoginTable.RootLoginEntry.COLUMN_NAME_USER + " = ? AND " + RootLoginTable.RootLoginEntry.COLUMN_NAME_PASSWORD + " = ?";
        String[] WHERE_ARGS = { user, pass };

        Cursor cursor = db.query(
                RootLoginTable.RootLoginEntry.TABLE_NAME,
                null,
                WHERE_CONDITION,
                WHERE_ARGS,
                null,
                null,
                null
        );

        if(cursor.moveToFirst()){
            db.close();
            return true;
        }else{
            db.close();
            return false;
        }
    }
}
