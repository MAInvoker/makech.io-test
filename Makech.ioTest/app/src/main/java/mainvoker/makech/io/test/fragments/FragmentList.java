package mainvoker.makech.io.test.fragments;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mainvoker.makech.io.test.R;
import mainvoker.makech.io.test.adapters.UsersAdapter;
import mainvoker.makech.io.test.logic.UsersLogic;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentList.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentList extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentList() {
        // Required empty public constructor
    }

    public RecyclerView lst_active_users;
    public RecyclerView lst_deactivated_users;
    public UsersAdapter adapterActive;
    public UsersAdapter adapterDeactivated;
    public GridLayoutManager layoutManagerActiveUsers;
    public GridLayoutManager layoutManagerDeactivatedUsers;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentList.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentList newInstance(String param1, String param2) {
        FragmentList fragment = new FragmentList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_listado, container, false);
        lst_active_users = view.findViewById(R.id.lsv_active_users);
        lst_deactivated_users = view.findViewById(R.id.lsv_deactivated_users);
        layoutManagerActiveUsers = new GridLayoutManager(getActivity(),1);
        layoutManagerDeactivatedUsers = new GridLayoutManager(getActivity(),1);
        layoutManagerActiveUsers.setOrientation(GridLayoutManager.VERTICAL);
        layoutManagerDeactivatedUsers.setOrientation(GridLayoutManager.VERTICAL);
        lst_active_users.setLayoutManager(layoutManagerActiveUsers);
        lst_deactivated_users.setLayoutManager(layoutManagerDeactivatedUsers);
        UsersLogic helper = new UsersLogic(getActivity());
        adapterActive = new UsersAdapter(getActivity(), helper.GetActiveUsers(),true);
        adapterDeactivated = new UsersAdapter(getActivity(), helper.GetDeactivatedUsers(),false);
        lst_active_users.setAdapter(adapterActive);
        lst_deactivated_users.setAdapter(adapterDeactivated);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
